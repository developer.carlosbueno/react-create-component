const { capitalize } = require("./utils/capitalize");

// component.tsx
exports.component = (name) => `import React from 'react';
import './${name}.scss';

export interface ${capitalize(name)}Props {}

const ${capitalize(name)} = ({}: ${capitalize(name)}Props) => {
  return <div>Hello 👋, I am a ${name} component.</div>;
};

export default ${capitalize(name)};
`;

// component.stories.jsx
exports.story = (name) => `import React from 'react';
import ${name} from './${name}.tsx';
export default {
  title: '${name}',
  component: ${name},
};
export const Default = () => <${name} />;
`;

// component.test.tsx
exports.test = (name) => `import React from 'react';
import { render } from '@testing-library/react';
import ${name} from './${name}';
describe('${name} Component', () => {
  test('it should match the snapshot', () => {
    const { asFragment } = render(<${name} />);
    expect(asFragment()).toMatchSnapshot();
  });
});
`;

// index.ts
exports.barrel = (name) => `import ${capitalize(name)} from './${capitalize(
  name
)}';

export default ${capitalize(name)};
`;

// Context
exports.context = (name) => {
  let str = capitalize(name);

  return `import { FC, ReactNode, createContext, useContext } from "react";

interface InitialProps {}

let initialState: InitialProps = {};

const ${str}Context = createContext<InitialProps>(initialState);

interface ${str}ProviderProps {
  children: ReactNode;
}

export const ${str}Provider: FC<${str}ProviderProps> = ({ children }) => {
  return (
    <${str}Context.Provider value={{}}>{children}</${str}Context.Provider>
  );
};

export const use${str}State = () => {
  return useContext(${str}Context);
};

export default ${str}Provider;`;
};

exports.service = () => `export const firstService = () => {};`;

exports.model = (name) => `export interface I${capitalize(name)}{};`;
