const fs = require("fs");
const {
  component,
  barrel,
  context,
  service,
  model,
} = require("./component_templates.js");
const { capitalize } = require("./utils/capitalize.js");

// grab component name from terminal argument
const [path] = process.argv.slice(2);
if (!path) throw new Error("You must include a component name.");

const [name] = process.argv.slice(3);
if (!name) throw new Error("You must include a component name.");

const dir = `./src/${path}/${name}/`;

// throw an error if the file already exists
if (fs.existsSync(dir))
  throw new Error("A component with that name already exists.");

// create the folders
fs.mkdirSync(dir);
fs.mkdirSync(dir + "components");
fs.mkdirSync(dir + "context");
fs.mkdirSync(dir + "services");
fs.mkdirSync(dir + "models");

function writeFileErrorHandler(err) {
  if (err) throw err;
}

// component.tsx
fs.writeFile(
  `${dir}/${capitalize(name)}.tsx`,
  component(name),
  writeFileErrorHandler
);
// component.scss
fs.writeFile(`${dir}/${name}.css`, "", writeFileErrorHandler);
// Context
fs.writeFile(`${dir}/context/index.tsx`, context(name), writeFileErrorHandler);
// Service
fs.writeFile(`${dir}/services/index.ts`, service(), writeFileErrorHandler);
// Model
fs.writeFile(`${dir}/models/index.ts`, model(name), writeFileErrorHandler);
// Barrel
fs.writeFile(`${dir}/index.ts`, barrel(name), writeFileErrorHandler);
